console.log("Hello from bot");

//https://github.com/ttezel/twit
let Twit = require('Twit');

let config = require('./config');
let T = new Twit(config);

let exec = require('child-process').exec;
let fs = require('fs');


// Setting up a user stream
let stream = T.stream('user');

// Anytime someone follows me
stream.on('follow', followed);

function tweetEvent(eventMsg){
	let fs = require('fs');
	let json = JSON.stringify(eventMsg,null,2);
	fs.writeFile("tweet.json", json);
}

// function followed(eventMsg){
// 	let name = eventMsg.source.name;
// 	let screenName = eventMsg.source.screen_name;
// 	tweetIt('.@' + screen_name + ' do you like spaghetti?');	
// }

//---------------------
tweetIt();
// setInterval( tweetIt, 1000*15);

function tweetIt(msg){

	//command to run processing and generate random .png image
	let cmd = 'processing-java --sketch=`pwd` /specific --run';
	exec(cmd,processing);

	function processing(){
		let filename = 'specific/output.png';
		let params = {
			encoding : 'base64'
		}
		let b64 = fs.readFileSync(filename,params);
		
		T.post('media/upload', {media_data : b64}, uploaded);

		function uploaded(err, data, response) {
			let id = data.media_id_string;
			let tweet = {
				status: '#coding from node',
				media_ids: [id]
			} 
			T.post('statuses/update', tweet, tweeted);
		}

		console.log("finished processing");
	}
	
	// let r = Math.floor(Math.random() * 100);
	// let tweet = {
	// 	status : msg
	// }

	// T.post('statuses/update', tweet , tweeted)

	 function tweeted (err, data, response) {
	 	// if(err){
	 	// 	console.log("Something went worng : " + err);
	 	// } else {
	 		console.log("data: " + data);		
	 	// }
	}
}

//---------------------get
// let params = {
// 	q: 'rolls',
// 	count: 2
// }

// T.get('search/tweets', params, gotData);

// function gotData(err, data, response){
// 	let tweets = data.statuses;
// 	for( let i = 0; i < tweets.length; i++){
// 		console.log(tweets[i].text);
// 	}
// 	// console.log(data);
// }
// /